# Mkdocs Material template with pdf conversion through pandoc and Eisvogel LaTeX

[![pipeline status](https://lab.gsi.upm.es/docs/mkdocs-material/badges/main/pipeline.svg)](https://lab.gsi.upm.es/docs/mkdocs-material/-/commits/main) 

## Introduction

The purpose of this repository is to show a template to **document a project with mkdocs and generate a pdf using pandoc and a latex template**. The **theme of mkdocs is Material for MkDocs** and recommended **latex template is Eisvogel**.

## Quick start

### Deploy on localhost

1. Clone this repository `git clone https://lab.gsi.upm.es/docs/mkdocs-material` and detele git dependencies with `rm -rf .git` or create a new mkdocs project with `mkdocs new <name-project>` with the same structure and copy the folder `latex-templates` and the files `pdf_metadata.md` and `mkdocs.yml`.
2. Serve the docs running in Docker with the image [jtr0/mkdocs-material](https://hub.docker.com/r/jtr0/mkdocs-material) (recommended because it has installed [mkdocs-pandoc-plugin](https://pypi.org/project/mkdocs-pandoc-plugin/) and [mkdocs-git-revision-date-localized-plugin](https://github.com/timvink/mkdocs-git-revision-date-localized-plugin)). Other option is to use the oficial image [squidfunk/mkdocs-material](https://hub.docker.com/r/squidfunk/mkdocs-material/) but without pdf generator and it's neccesary to comment `pandoc` and `git-revision-date-localized` options in [mkdocs.yml](mkdocs.yml). Using an image with plugins:
```bash
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs jtr0/mkdocs-material
```
3. Open [http://localhost:8000/](http://localhost:8000/) in your browser.

### Build pdfs with pandoc and Eisvogel LaTeX template in local

1. Build public content with pandoc-plugin to combine all markdown files in one (site/docs.pdf.md):
```bash
docker run --rm -it -v ${PWD}:/docs -e ENABLE_PANDOC_EXPORT=1 jtr0/mkdocs-material build
```

2. Build pdf with pandoc and eisvogel LaTeX:
```bash
docker run --rm -it -v ${PWD}:/data  jtr0/latex-pandoc /bin/bash -c 'cp -r /data/docs/imgs /imgs  ; pandoc -N --template=eisvogel.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 /data/pdf_metadata.md /data/site/docs.pdf.md  --pdf-engine=xelatex --toc -o /data/eisvogel_template.pdf;' 
```

### Deploy on GitLab Pages

1. Go to GitLab, select `Set CI/CD` and copy the content of [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Overview

### Pdf
| Title page | Content | 
| :----:      |    :----:   | 
| ![Title page of PDF](docs/imgs/eisvogel_title_page.png)   | ![Content page of PDF](docs/imgs/eisvogel_content.png)    | 

| Images   | Chapther One  |
|   :----:       |  :----:       |
|![Images](docs/imgs/eisvogel_images.png) |![Chapther One](docs/imgs/eisvogel_chapter1.png)  |

Download artifacts of pdfs: [https://lab.gsi.upm.es/docs/mkdocs-material/-/jobs/artifacts/main/download?job=generate_pdfs](https://lab.gsi.upm.es/docs/mkdocs-material/-/jobs/artifacts/main/download?job=generate_pdfs)

### Web page

![Material for Mkdocs](docs/imgs/web_page.png)

Accesible on: [https://docs.gsi.upm.es/docs/mkdocs-material/](https://docs.gsi.upm.es/docs/mkdocs-material/)

### Mkdocs
[Mkdocs](https://www.mkdocs.org/) is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. Documentation source files are written in Markdown, and configured with a single YAML configuration.

### Material for MkDocs

[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) is a theme for MkDocs, a static site generator geared towards (technical) project documentation. 

### Pandoc

[Pandoc](https://pandoc.org/) is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library.

### Eisvogel

[Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) is a clean pandoc LaTeX template to convert your markdown files to PDF or LaTeX. It is designed for lecture notes and exercises with a focus on computer science. See the [custom templates variables](https://github.com/Wandmalfarbe/pandoc-latex-template#custom-template-variables).

## Plugins

By default, [mkdocs-minify-plugin](https://github.com/byrnereese/mkdocs-minify-plugin) and [mkdocs-redirects](https://github.com/datarobot/mkdocs-redirects) are installed in oficial docker image [squidfunk/mkdocs-material](https://hub.docker.com/r/squidfunk/mkdocs-material/).

### mkdocs-pandoc-plugin

Use [mkdocs-pandoc-plugin](https://pypi.org/project/mkdocs-pandoc-plugin/) and generate a pdf or other output with pandoc. Active the opction with the environmental variable `ENABLE_PANDOC_EXPORT` set to 1.

### mkdocs-git-revision-date-localized-plugin

Use [mkdocs-git-revision-date-localized-plugin](https://github.com/timvink/mkdocs-git-revision-date-localized-plugin) to enable displaying the date of the last git modification of a page.

## Deploy

### Gitlab Pages

- Create a `.gitlab-ci.yml` with the following content:
```yaml
stages:
  - build
  - deploy
  - pdfs 

build_mkdocs:
  image: jtr0/mkdocs-material
  stage: build 
  only:
    - main
  script:
    - export ENABLE_PANDOC_EXPORT=1
    - mkdir data
    - mkdocs build --verbose --site-dir data --no-directory-urls 
  artifacts:
    paths:
      - data

pages:
  image: jtr0/mkdocs-material
  stage: deploy
  only:
    - main
  script:
    - mkdocs build --verbose --site-dir public --no-directory-urls 
  artifacts:
    paths:
      - public

generate_pdfs:
  image: jtr0/latex-pandoc
  stage: pdfs
  only:
    - main
  script:
    - mkdir public/pdfs/
    - cp -r docs/imgs imgs
    - pandoc -N --template=eisvogel.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 pdf_metadata.md data/docs.pdf.md  --pdf-engine=xelatex --toc -o public/pdfs/eisvogel_template.pdf
    - pandoc -N --template=pandoc-scholar.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 pdf_metadata.md data/docs.pdf.md  --pdf-engine=xelatex --toc -o public/pdfs/pandoc-scholar_template.pdf
    - pandoc -N --template=default.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 pdf_metadata.md data/docs.pdf.md  --pdf-engine=xelatex --toc -o public/pdfs/default_template.pdf
  artifacts:
    paths:
      - public/pdfs/
```

### GitHub Pages

- Deploy documentation to GitHub Pages (not tested):
```bash
docker run --rm -it -v ~/.ssh:/root/.ssh -v ${PWD}:/docs squidfunk/mkdocs-material gh-deploy 
```

