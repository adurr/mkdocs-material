#!/bin/sh

docker build -f dockerfiles/mkdocs-material+plugins.Dockerfile -t jtr0/mkdocs-material .

docker build --file dockerfiles/latex-pandoc.Dockerfile -t jtr0/latex-pandoc .
