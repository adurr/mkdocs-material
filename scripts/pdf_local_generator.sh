#!/bin/sh

# Build public content with pandoc-plugin to combine markdown files
docker run --rm -it -v ${PWD}:/docs -e ENABLE_PANDOC_EXPORT=1 jtr0/mkdocs-material build

# Generate templates pdfs 
docker run --rm -it -v ${PWD}:/data  jtr0/latex-pandoc /bin/bash -c 'cp -r /data/docs/imgs /imgs  ; pandoc -N --template=eisvogel.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 /data/pdf_metadata.md /data/site/docs.pdf.md  --pdf-engine=xelatex --toc -o /data/pdfs-examples/eisvogel_template.pdf;    pandoc -N --template=pandoc-scholar.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 /data/pdf_metadata.md /data/site/docs.pdf.md  --pdf-engine=xelatex --toc -o /data/pdfs-examples/pandoc-scholar_template.pdf; pandoc -N --template=default.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 /data/pdf_metadata.md /data/site/docs.pdf.md  --pdf-engine=xelatex --toc -o /data/pdfs-examples/default_template.pdf' 
